<?php declare(strict_types=1);

return [
    'doctrine' => [

        'connection' => [
            'orm_default' => [
                'params' => [
                    // !!! set connection parameters in the ./local.php config file !!!
                ],
            ],
        ],

    ],
];

