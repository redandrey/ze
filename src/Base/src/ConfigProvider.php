<?php

declare(strict_types=1);

namespace Base;

use Base\DbTypes\Money;
use Base\Factory\HandlerAbstractFactory;

/**
 * The configuration provider for the Base module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'doctrine'     => $this->getDoctrineConfig(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
            ],
            'abstract_factories' => [
                new HandlerAbstractFactory(),
            ],
        ];
    }

    public function getDoctrineConfig()
    {
        return [
            // user-defined types
            'types' => [
                Money::getDbName() => Money::class,
            ],

            // where to find entities and how to process them
            'driver' => [
                'orm_default' => [
                    'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                    'drivers' => [
                        'Base\DbTypes' => 'base_db_types',
                    ],
                ],
                'base_db_types' => [
                    'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => __DIR__ . '/DbTypes',
                ],
            ],
        ];
    }

}
