<?php


namespace Base\DbTypes;


use Doctrine\DBAL\Platforms\AbstractPlatform;

class AbstractEnum extends AbstractType
{

    /** @var array  */
    protected const VALUES = [];

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) : string
    {
        $values = "'" . implode("', '", static::VALUES) . "'";

        return sprintf(
            'ENUM(%s) COMMENT \'(DC2Type:%s)\'',
            $values,
            $this->getName()
        );
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!in_array($value, static::VALUES)) {
            throw new \InvalidArgumentException('Invalid value: ' . $value);
        }

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function getMappedDatabaseTypes(AbstractPlatform $platform)
    {
        return [
            'enum',
        ];
    }

    public static function hasValue(string $value) : bool
    {
        return in_array($value, static::VALUES);
    }
}
