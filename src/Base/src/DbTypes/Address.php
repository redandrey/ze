<?php


namespace Base\DbTypes;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable() */
class Address implements \JsonSerializable
{

    /**
     * @ORM\Column(name="city", type="string", length=30, nullable=true)
     * @var string
     */
    protected $city;

    /**
     * @ORM\Column(name="state", type="string", length=2, nullable=true)
     * @var string
     */
    protected $state;

    /**
     * @ORM\Column(name="zip_code", type="string", length=3, nullable=true)
     * @var string
     */
    protected $zipCode;

    /**
     * @ORM\Column(name="address", type="string", length=250, nullable=true)
     * @var string
     */
    protected $address;

    public function __construct(
        string $address,
        string $city = null,
        string $state = null,
        string $zipCode = null
    ) {
        $this->address = $address;
        $this->city = $city;
        $this->state = $state;
        $this->zipCode = $zipCode;
    }

    public function jsonSerialize()
    {
        return [
            'state' => $this->state,
            'city' => $this->city,
            'zip' => $this->zipCode,
            'address' => $this->address,
        ];
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

}
