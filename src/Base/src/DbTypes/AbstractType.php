<?php


namespace Base\DbTypes;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

abstract class AbstractType extends Type
{
    /**
     * Returns class name without namespace as a type name
     *
     * @return string
     */
    public static function getDbName() : string
    {
        $className = static::class;
        $lastSlashPos = strrpos($className, '\\');
        $startPos = false === $lastSlashPos ? 0 : $lastSlashPos + 1;

        return substr($className, $startPos);
    }

    public function getName() : string
    {
        return static::getDbName();
    }

    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     *
     * @return string|void
     * @throws \Exception
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        throw new \Exception('Realize it in the your concrete class');
    }

}
