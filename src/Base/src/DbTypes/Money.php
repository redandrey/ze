<?php


namespace Base\DbTypes;


use Doctrine\DBAL\Platforms\AbstractPlatform;

class Money extends AbstractType
{

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return sprintf(
            'DECIMAL(18,2) COMMENT \'(DC2Type:%s)\'',
            $this->getName()
        );
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (float)$value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \Base\ValueObjects\Money((float) $value);
    }

}
