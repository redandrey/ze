<?php


namespace Base\ValueObjects;


class Money
{

    /** @var float */
    protected $value;

    public function __construct(float $value)
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    public function formatted() : string
    {
        return sprintf('$%.2f', $this->value);
    }

}
