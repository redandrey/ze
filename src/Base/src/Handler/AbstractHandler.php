<?php


namespace Base\Handler;


use Doctrine\ORM\EntityManager;

abstract class AbstractHandler
{

    /** @var EntityManager  */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns Doctrine Entity Manager
     *
     * @return EntityManager
     */
    protected function em()
    {
        return $this->em;
    }

}
