<?php


namespace Base\Factory;


use Psr\Http\Server\RequestHandlerInterface;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

class HandlerAbstractFactory implements AbstractFactoryInterface
{

    public function canCreate(\Interop\Container\ContainerInterface $container, $requestedName)
    {
        if (!class_exists($requestedName)) {

            return false;
        }

        return in_array(
            RequestHandlerInterface::class,
            class_implements($requestedName, true)
        );
    }

    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get('doctrine.entity_manager.orm_default')
        );
    }

}
