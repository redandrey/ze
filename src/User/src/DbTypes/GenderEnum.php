<?php


namespace User\DbTypes;


use Base\DbTypes\AbstractEnum;

class GenderEnum extends AbstractEnum
{

    const FEMALE = 'female';
    const MALE = 'male';

    protected const VALUES = [
        self::MALE,
        self::FEMALE,
    ];

}
