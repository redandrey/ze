<?php


namespace User\DbTypes;


use Base\DbTypes\AbstractEnum;

class PhoneTypeEnum extends AbstractEnum
{

    const HOME_PHONE = 'home';
    const WORK_PHONE = 'work';
    const CELL_PHONE = 'cell';
    const OTHER_PHONE = 'other';
    const EMERGENCY_PHONE = 'emergency';

    protected const VALUES = [
        self::HOME_PHONE,
        self::WORK_PHONE,
        self::CELL_PHONE,
        self::OTHER_PHONE,
        self::EMERGENCY_PHONE,
    ];

}
