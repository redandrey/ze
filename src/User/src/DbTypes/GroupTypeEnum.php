<?php


namespace User\DbTypes;


use Base\DbTypes\AbstractEnum;

class GroupTypeEnum extends AbstractEnum
{

    const MEMBERSHIP = 'membership';
    const GUEST = 'guest';
    const ADDON = 'addon';

    protected const VALUES = [
        self::MEMBERSHIP,
        self::GUEST,
        self::ADDON,
    ];

}
