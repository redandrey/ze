<?php

declare(strict_types=1);

namespace User;

use Base\Factory\HandlerAbstractFactory;
use User\Handler\UserAddHandler;
use User\Handler\UserListHandler;

/**
 * The configuration provider for the User module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'doctrine'     => $this->getDoctrineConfig(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
                UserListHandler::class => HandlerAbstractFactory::class,
                UserAddHandler::class => HandlerAbstractFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'user' => [__DIR__ . '/../templates/'],
            ],
        ];
    }

    /**
     * Returns doctrine config
     *
     * @return array
     */
    public function getDoctrineConfig() : array
    {
        return [
            // user-defined types
            'types' => [
                DbTypes\GenderEnum::getDbName()    => DbTypes\GenderEnum::class,
                DbTypes\GroupTypeEnum::getDbName() => DbTypes\GroupTypeEnum::class,
                DbTypes\PhoneTypeEnum::getDbName() => DbTypes\PhoneTypeEnum::class,
            ],

            // where to find entities and how to process them
            'driver' => [
                'orm_default' => [
                    'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                    'drivers' => [
                        'User\Entity' => 'user_entities',
                    ],
                ],
                'user_entities' => [
                    'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => __DIR__ . '/Entity',
                ],
            ],
        ];
    }

}
