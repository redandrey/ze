<?php


namespace User\Repository;


use Doctrine\ORM\EntityRepository;
use User\Entity\User;
use User\Exception\UserNotFoundException;

class UserRepository extends EntityRepository
{

    /**
     * @param User|int $user
     *
     * @return mixed
     * @throws UserNotFoundException
     */
    public function getFamily($user) : array
    {
        if (is_scalar($user)) {
            $user = $this->_em->find(User::class, $user);
        }

        if (!$user instanceof User) {
            throw new UserNotFoundException();
        }

        $familyHolder = $user->getFamilyHolder();

        $builder = $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.id = :user')
            ->orWhere('u.primaryUser = :user')
            ->orWhere('u.parentUser = :user')
            ->setParameter('user', $familyHolder);

        return $builder->getQuery()->getResult();
    }

}
