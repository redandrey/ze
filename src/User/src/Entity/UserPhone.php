<?php


namespace User\Entity;


use Doctrine\ORM\Mapping as ORM;
use User\DbTypes\PhoneTypeEnum;

/**
 * Class UserContact
 * @package User\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="user_phone")
 */
class UserPhone implements \JsonSerializable
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="phones", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(name="phone_type", type="PhoneTypeEnum")
     * @var string
     */
    protected $phoneType;

    /**
     * @ORM\Column(name="phone_number", type="string")
     * @var string
     */
    protected $phoneNumber;

    /**
     * UserPhone constructor.
     *
     * @param string $type
     * @param string $number
     */
    public function __construct(string $type, string $number)
    {
        $this->setType($type);
        $this->setNumber($number);
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'type' => $this->phoneType,
            'number' => $this->phoneNumber,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->phoneType;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        if (!PhoneTypeEnum::hasValue($type)) {
            throw new \InvalidArgumentException(sprintf(
                'Enum %s has no value \'%s\'',
                static::class,
                $type
            ));
        }

        $this->phoneType = $type;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->phoneNumber = $number;
    }

}
