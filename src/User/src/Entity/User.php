<?php


namespace User\Entity;


use Base\DbTypes\Address;
use Base\ValueObjects\Money;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Location\Entity\Location;
use User\DbTypes\GenderEnum;

/**
 * Class User
 * @package User\Entity
 *
 * @ORM\Entity(repositoryClass="User\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements \JsonSerializable
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @var bool
     */
    protected $isActive;

    /**
     * @ORM\Column(name="gender", type="GenderEnum")
     * @var string
     */
    protected $gender;

    /**
     * @ORM\Column(name="login", type="string", length=30)
     * @var string
     */
    protected $login;

    /**
     * @ORM\Column(name="email", type="string", length=30)
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="first_name", type="string", length=30)
     * @var string
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=30)
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(name="account_num", type="string", length=30, nullable=true)
     * @var string
     */
    protected $accessCardNumber;

    /**
     * @ORM\Column(type="Money", nullable=false)
     * @var Money
     */
    protected $balance;

    /**
     * @ORM\Embedded(class="Base\DbTypes\Address", columnPrefix="")
     * @var Address|null
     */
    protected $address;

    /**
     * @ORM\ManyToMany(targetEntity="User\Entity\Group", inversedBy="users")
     * @ORM\JoinTable(
     *     name="users_groups",
     *     joinColumns={
     *          @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     *     }
     * )
     * @var Collection
     */
    protected $groups;

    /**
     * @ORM\ManyToMany(targetEntity="Location\Entity\Location", inversedBy="users")
     * @ORM\JoinTable(name="user_favorite_locations")
     * @var Collection
     */
    protected $favoriteLocations;

    /**
     * @ORM\OneToMany(targetEntity="UserPhone", mappedBy="user", cascade={"all"})
     * @var Collection
     */
    protected $phones;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="children")
     * @ORM\JoinColumn(name="primary_id", referencedColumnName="id", nullable=true)
     * @var User|null
     */
    protected $primaryUser;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="primaryUser")
     * @var Collection
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="secondaries")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * @var User|null
     */
    protected $parentUser;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="parentUser")
     * @var Collection
     */
    protected $secondaries;


    public function __construct()
    {
        $this->groups      = new ArrayCollection();
        $this->phones      = new ArrayCollection();
        $this->children    = new ArrayCollection();
        $this->secondaries = new ArrayCollection();
    }

    public function getFamilyHolder() : User
    {
        if (null !== $this->primaryUser) {

            return $this->primaryUser;
        }
        if (null !== $this->parentUser) {

            return $this->parentUser;
        }

        return $this;
    }

    public function isSecondary() : bool
    {
        return null !== $this->primaryUser;
    }

    public function isDependant() : bool
    {
        return null !== $this->parentUser;
    }

    public function isFamilyHolder()
    {
        return null === $this->primaryUser && null === $this->parentUser;
    }

    public function getPrimary() : User
    {
        return $this->primaryUser;
    }

    public function getParent() : User
    {
        return $this->parentUser;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @return Collection
     */
    public function getSecondaries(): Collection
    {
        return $this->secondaries;
    }

    public function addSecondary(User $user) : void
    {
        $user->parentUser = $this;
        $this->secondaries[] = $user;
    }

    public function addChildren(User $user) : void
    {
        $user->primaryUser = $this;
        $this->children[] = $user;
    }

    public function getPhones()
    {
        return $this->phones;
    }

    public function addPhone(UserPhone $phone) : void
    {
        $phone->setUser($this);
        $this->phones->add($phone);
    }

    public function setPhones(Collection $phones) : void
    {
        $this->phones = $phones;
    }

    /**
     * @return Collection
     */
    public function getFavoriteLocations(): Collection
    {
        return $this->favoriteLocations;
    }

    public function addFavoriteLocation(Location $location) : void
    {
        if ($this->favoriteLocations->contains($location)) {

            return;
        }

        $this->favoriteLocations[] = $location;
    }

    public function jsonSerialize()
    {
        return [
            'id'        => $this->id,
            'isActive'  => $this->isActive,
            'gender'    => $this->gender,
            'login'     => $this->login,
            'firstName' => $this->firstName,
            'lastName'  => $this->lastName,
            'balance'   => $this->balance->formatted(),
            'address'   => $this->address,
            'groups'    => $this->groups->toArray(),
            'locations' => $this->favoriteLocations->toArray(),
            'phones'    => $this->phones->toArray(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function activate() : void
    {
        $this->isActive = true;
    }

    public function deactivate() : void
    {
        $this->isActive = false;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return Address
     */
    public function getAddress() : Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAccessCardNumber(): string
    {
        return $this->accessCardNumber;
    }

    /**
     * @param string $accessCardNumber
     */
    public function setAccessCardNumber(string $accessCardNumber): void
    {
        $this->accessCardNumber = $accessCardNumber;
    }

    /**
     * @return Collection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * Add group to the user
     *
     * @param Group $group
     */
    public function addGroup(Group $group) : void
    {
        if ($this->groups->contains($group)) {

            return;
        }

        $this->groups[] = $group;
    }

    /**
     * @param Collection $groups
     */
    public function setGroups(Collection $groups): void
    {
        $this->groups = $groups;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        if (!GenderEnum::hasValue($gender)) {

            throw new \InvalidArgumentException();
        }
        $this->gender = $gender;
    }

    /**
     * @return Money
     */
    public function getBalance(): Money
    {
        return $this->balance;
    }

    /**
     * @param Money $balance
     */
    public function setBalance(Money $balance): void
    {
        $this->balance = $balance;
    }

}
