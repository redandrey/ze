<?php


namespace User\Handler;


use Base\Handler\AbstractHandler;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use User\Entity\User;
use User\Exception\UserNotFoundException;
use User\Repository\UserRepository;
use Zend\Diactoros\Response\JsonResponse;

class UserListHandler extends AbstractHandler implements RequestHandlerInterface
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $params = $request->getQueryParams();
        if (!array_key_exists('userId', $params)) {

            return new JsonResponse([
                'error' => 'userId query parameter is required'
            ], 400);
        }
        $userId = $params['userId'];

        /** @var UserRepository $userRepo */
        $userRepo = $this->em()->getRepository(User::class);
        try {
            $users = $userRepo->getFamily($userId);
        } catch (UserNotFoundException $e) {
            return new JsonResponse([
                'error' => sprintf('User with id=%s not found', $userId)
            ], 404);
        }

        return new JsonResponse([
            'users' => $users,
        ]);
    }

}
