<?php


namespace User\Handler;


use Base\DbTypes\Address;
use Base\Handler\AbstractHandler;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use User\DbTypes\GenderEnum;
use User\DbTypes\PhoneTypeEnum;
use User\Entity\User;
use User\Entity\UserPhone;
use Zend\Diactoros\Response\JsonResponse;

class UserAddHandler extends AbstractHandler implements RequestHandlerInterface
{

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $user = new User();
        $user->activate();
        $user->setGender(GenderEnum::MALE);
        $user->setLogin('ared');
        $user->setEmail('ared@localhost');
        $user->setFirstName('andy');
        $user->setLastName('red');
        $user->setAddress(new Address(''));

        $user->addPhone(new UserPhone(
            PhoneTypeEnum::HOME_PHONE,
            '555123321'
        ));

        $this->em()->flush();

        return new JsonResponse([
            'user' => $user,
            'user-phones' => $user->getPhones(),
        ]);
    }

}
