# Zend Expressive + Doctrine

- - - -

Реализована модульная структура приложения. Каждый модуль имеет свою [конфигурацию](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/ConfigProvider.php?at=master&fileviewer=file-view-default).

- - - -

В хендлере [App\Handler\HomePageHandler](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/App/src/Handler/HomePageHandler.php?at=master&fileviewer=file-view-default) можно посмотреть пример HTML-ответа, в хендлере [User\Handler\UserListHandler](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Handler/UserListHandler.php?at=master&fileviewer=file-view-default) - API.

- - - -
Добавлен кастомный тип доктрины [Base\DbTypes\Money](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/Base/src/DbTypes/Money.php?at=master&fileviewer=file-view-default). 
На стороне базы он хранится как DECIMAL(18,2), на стороне PHP предстабляет собой Value Object [Base\ValueObjects\Money](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/Base/src/ValueObjects/Money.php?at=master&fileviewer=file-view-default). Тип используется для поля [balance](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/User.php?at=master&fileviewer=file-view-default#User.php-75) в сущности User:

```php
    /**
     * @ORM\Column(type="Money", nullable=false)
     * @var Money
     */
    protected $balance;
```
- - - -

Добавлен составной тип [Address](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/Base/src/DbTypes/Address.php?at=master&fileviewer=file-view-default). Используеся для поля [address](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/User.php?at=master&fileviewer=file-view-default#User.php-81) в сущности User:

~~~
    /**
     * @ORM\Embedded(class="Base\DbTypes\Address", columnPrefix="")
     * @var Address|null
     */
    protected $address;
~~~
- - - - 
В классе [Base\DbTypes\AbstractEnum](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/Base/src/DbTypes/AbstractEnum.php?at=master&fileviewer=file-view-default) реализован маппинг ENUM'ов. На примере типа телефонного номера:



    /**
     * @ORM\Entity()
     * @ORM\Table(name="user_phone")
     */
    class UserPhone implements \JsonSerializable
    {
        /**
         * @ORM\Column(name="phone_type", type="PhoneTypeEnum")
         * @var string
         */
        protected $phoneType;
	}

    class PhoneTypeEnum extends AbstractEnum
    {
    
        const HOME_PHONE = 'home';
        const WORK_PHONE = 'work';
        const CELL_PHONE = 'cell';
        const OTHER_PHONE = 'other';
        const EMERGENCY_PHONE = 'emergency';

        protected const VALUES = [
            self::HOME_PHONE,
            self::WORK_PHONE,
            self::CELL_PHONE,
            self::OTHER_PHONE,
            self::EMERGENCY_PHONE,
        ];
    }

- - - - 

Связи между сущностями:

* [User\Entity\User](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/User.php?at=master&fileviewer=file-view-default) <-> [Location\Entity\Location](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/Location/src/Entity/Location.php?at=master&fileviewer=file-view-default) (много ко многим)
* [User\Entity\User](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/User.php?at=master&fileviewer=file-view-default) <-> [User\Entity\Group](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/Group.php?at=master&fileviewer=file-view-default#Group.php-41) (много ко многим)
* [User\Entity\User](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/User.php?at=master&fileviewer=file-view-default) <-> [User\Entity\UserPhone](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/UserPhone.php?at=master&fileviewer=file-view-default#UserPhone.php-28) (один ко многим)
* Связи пользователя с праймари и парентом (один ко многим, self-referenced) [тынц](https://bitbucket.org/redandrey/ze/src/e87e098eca4345f1617580fa2fe19e9df409c634/src/User/src/Entity/User.php?at=master&fileviewer=file-view-default#User.php-115).

- - - -

Все сущности реализуют интерфейс JsonSerializable, поэтому преобразование в JSON происходит автоматически:

~~~php
    $users = $usersRepo->getFamily($userId);
	return new JsonResponse([
        'users' => $users,
    ]);
~~~
